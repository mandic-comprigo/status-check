<?php

namespace Comprigo;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;




class StatusCheckerCommand extends Command
{
    protected $csvFile;

    protected function configure()
    {
        $this
            ->setName('status-check')
            ->setDescription('Checks a list of urls for 5xx status codes')
            ->addArgument('path', InputArgument::REQUIRED, 'The path to the CSV file.')
            ->addOption('all', null, InputOption::VALUE_NONE, 'Display all status codes, except 200')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Checking URLs',
            '============',
            '',
        ]);

        $this->setFile($input->getArgument('path'));
        if($input->getOption('all'))
        {
            $this->statusCheckAll();
        }
        else
        {
            $this->statusCheck5xx();
        }
    }


    public function setFile($path)
    {
        $this->csvFile = file($path);
    }

    //return only the URLs that have a status response of 5xx e.g. 500, 501...
    public function statusCheck5xx()
    {
        foreach ($this->csvFile as $line) {
            $data[] = str_getcsv($line);
        }

        for($i=1; $i < count($data); $i ++)
        {
            $status = get_headers($data[$i][0])[0];
            if(preg_match('/HTTP\/1\.\d 5.*/', $status))
            {
                echo $data[$i][0]."  ".$status."\n";

            }
        }
    }

    //return all the URLs that have a status response different than 200
    public function statusCheckAll()
    {
        foreach ($this->csvFile as $line) {
            $data[] = str_getcsv($line);
        }

        for($i=1; $i < count($data); $i ++)
        {
            $status = get_headers($data[$i][0])[0];
            if(!preg_match('/HTTP\/1\.\d 200 OK/', $status))
            {
                echo $data[$i][0]."  ".$status."\n";

            }
        }
    }
}