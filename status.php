<?php

require_once __DIR__.'/vendor/autoload.php';

use Symfony\Component\Console\Application;
use Comprigo\StatusCheckerCommand;


$command = new StatusCheckerCommand();
$application = new Application();
$application->add($command);
$application->run();