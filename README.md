##Used to check a CSV file containing URLs for HTTP response status.

- Clone repo
- Run composer install
- Run in terminal: php status.php status-check path_to_your_csv_file --all
- e.g. php status.php status-check /var/www/file.csv --all
- By default the script displays only 5xx status codes, but if you include the optional parameter --all, it will display all status codes different than 200
